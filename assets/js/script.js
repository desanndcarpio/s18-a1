console.log('Hello World');

/*


Mini-Activity:
 	1. Create an "activity" folder, an "index.html" file inside of it and link the "script.js" file.
	2. Create a "script.js" file and console log the message "Hello World" to ensure that the script file is properly associated with the html file.
	3. Create a "trainer" object by using object literals.
	4. Initialize/add the "trainer" object properties.
	5. Initialize/add the "trainer" object method.
	6. Access the "trainer" object properties using dot and square bracket notation.
	7 .Invoke/call the "trainer" object method.
	8. Create a constructor function for creating a pokemon.
	9. Create/instantiate several pokemon using the "constructor" function.
	10. Have the pokemon objects interact with each other by calling on the "tackle" method.



*/

/*
Create a "trainer" object by using object literals.
Initialize/add the "trainer" object properties.
Initialize/add the "trainer" object method.

*/
let Trainer = {
	firstName: "Ash",
	lastName: "Ketshum",
	age: 10,
	pokemon: "Pikachu",
	location: "Pallet Town",
	catch: function(){
		console.log("I choose you!");
	}
};

//Access the "trainer" object properties using dot and square bracket notation.
console.log(Trainer);
console.log(Trainer.firstName);
console.log(Trainer['pokemon']);


//Create a constructor function for creating a pokemon.
function Pokemon(name){
	this.name = name;
	this.health = 100;
	this.tackle = function(target){
		console.log(`${this.name} tackled ${target.name}`);
		target.health -= 10;
		console.log(`${target.name}'s health is now ${target.health}`);
		console.log(`${target.name} counter attacked ${this.name}`);
		this.health -= 20;
		console.log(`${this.name}'s health now is ${this.health}`);
	};
	this.super = function(target){ 
		console.log(`${this.name} used his super attack on ${target.name}. Very effective!!`);
		target.health /= 2;
		console.log(`${target.name}'s health is now ${target.health}!!`);

	};
	this.run = function(){
		console.log(`${this.name}'s health is very low. ${this.name} decided to escape.`)
	}
};

//Create/instantiate several pokemon using the "constructor" function.
let Pikachu = new Pokemon("Pikachu");
let Clefairy = new Pokemon("Clefairy");

//Have the pokemon objects interact with each other by calling on the "tackle" method.
Pikachu.tackle(Clefairy);
Clefairy.tackle(Pikachu);
Pikachu.super(Clefairy);
Clefairy.run();